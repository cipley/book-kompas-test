package com.cipley.kompastest.book.activities;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import karacken.curl.PageCurlAdapter;
import karacken.curl.PageSurfaceView;

/**
 * @author Dipo Santiko
 * @since 6/15/17
 */

public class BookActivity extends AppCompatActivity {
    private PageSurfaceView pageSurfaceView;
    private int screen_width;
    private int screen_height;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageSurfaceView = new PageSurfaceView(this);
        setContentView(pageSurfaceView);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        screen_width = size.x;
        screen_height = size.y;
        display.getSize(size);

        String[] res_array = new String[]{"01.png", "02.png", "03.png", "04.png", "05.png"};

        PageCurlAdapter pageCurlAdapter = new PageCurlAdapter(res_array);
        pageSurfaceView.setPageCurlAdapter(pageCurlAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageSurfaceView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pageSurfaceView.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return pageSurfaceView.onPageTouchEvent(event);
    }
}
